import './paper-search-bar.js';
import './paper-filter-dialog.js';
import './paper-search-panel.js';

/*
Workaround for https://github.com/PolymerElements/iron-component-page/issues/34

TODO: Remove this file once the ticket is implemented.
*/
/*
  FIXME(polymer-modulizer): the above comments were extracted
  from HTML and may be out of place here. Review them and
  then delete this comment!
*/
;
