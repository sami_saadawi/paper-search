/**
Panel with a search bar and filtering dialog.

### Example

```html
<paper-search-panel
	search="{{search}}"
	count="{{count}}"
	items="[[items]]"
	has-more="[[hasMore]]"
	loading="[[loading]]"
	filters="[[filters]]"
	selected-filters="{{selectedFilters}}"
	on-change-request-params="loadData">
	<div>
		Show your [[count]] results for "[[search]]"
	</div>
</paper-search-panel>
```

The panel shows the search configuration, and considers the `items` property to know whether
the lister is currently showing any results.

The content is hidden if no results are available. Content with attribute `fixed`
is shown even without no results (check the demo for an example).

@demo demo/paper-search-panel.html
*/
/*
  FIXME(polymer-modulizer): the above comments were extracted
  from HTML and may be out of place here. Review them and
  then delete this comment!
*/
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

import '@polymer/iron-flex-layout/iron-flex-layout.js';
import '@cwmr/paper-more-button/paper-more-button.js';
import '@polymer/paper-styles/color.js';
import './paper-search-bar.js';
import './paper-filter-dialog.js';

class PaperSearchPanel extends PolymerElement {
    
    static get template() {
        return html`
        <style include="iron-flex iron-flex-alignment">
            :host {
                display: block;
            }

            .horizontal-holder {
                background: var(--background-color, white);
                display: block;
                padding: 0 16px;
                @apply --layout-horizontal;
                @apply --layout-center-center;
                height: var(--paper-search-bar-height, 48px);
                box-sizing: border-box;
            }

            iron-input {
                @apply --layout-flex;
                @apply --layout-vertical;
                height: 100%;
            }

            .icon {
                color: var(--disabled-text-color);
                @apply --icon-styles;
            }

            #input {
                @apply --layout-flex;
                margin: 0 10px;
                padding: 16px 0;
                cursor: text;
                background: transparent;
                color: inherit;
                @apply --input-styles;
                border: 0;
                outline: 0;
            }
            #input::-ms-clear {
                display: none;
            }

            #input[disabled] {
                @apply --disabled-input-styles;
            }

            .badge {
                --paper-badge-background: var(--paper-red-500);
                --paper-badge-opacity: 1;
                --paper-badge-width: 18px;
                --paper-badge-height: 18px;
                --paper-badge-margin-left: -5px;
                --paper-badge-margin-bottom: -25px;
            }

            .badge[invisible] {
                visibility: hidden;
            }
        </style>

        <div class="horizontal-holder">
            <iron-icon icon="[[icon]]" class="icon"></iron-icon>
            <iron-input bind-value="{{query}}">
                <!-- Define is="iron-input" for backwards compatibility with Polymer 1.x -->
                <input id="input" is="iron-input" placeholder="[[placeholder]]" value="{{value::input}}"></input>
            </iron-input>

            <template is="dom-if" if="{{query}}">
                <paper-icon-button icon="clear" on-tap="_clear" class="icon"></paper-icon-button>
            </template>
            <template is="dom-if" if="{{!hideFilterButton}}">
                <template is="dom-if" if="{{!disableFilterButton}}">
                    <paper-icon-button id="filter" icon="image:tune" on-tap="_filter" class="icon"></paper-icon-button>
                </template>
                <paper-badge for="filter" label="[[nrSelectedFilters]]" class="badge" invisible$="[[!nrSelectedFilters]]"></paper-badge>
            </template>
        </div>`;
    }

    static get properties() {
        return {
            /**
             * Query for which the user was searching
             */
            search: {
                type: String,
                observer: '_onChangeRequest',
                notify: true
            },
            /**
             * All filters from which the user can choose
             */
            filters: Object,
            /**
             * All filters that have been selected by the user, e.g. `{ age: [ "child", "teen" ] }`
             */
            selectedFilters: {
                type: Object,
                observer: '_onChangeRequest',
                notify: true,
                value: {}
            },
            /**
             * Items that are currently shown in the lister
             */
            items: Array,
            /**
             * True if further items could be loaded
             */
            hasMore: {
                type: Boolean,
                value: false
            },

            /**
             * True if items are currently loaded
             */
            loading: {
                type: Boolean,
                value: false
            },

            /**
             * Whether to hide the Filter button. Set attribute "hide-filter-button" to do so.
             */
            hideFilterButton: {
                type: Boolean,
                value: false
            },

            /**
             * Number of items loaded per page (i.e. for each click on [more])
             */
            count: {
                type: Number,
                notify: true,
                value: 20
            },
            /**
             * Icon shown in the search background
             */
            icon: {
                type: String,
                value: 'search'
            },
            /**
             * Text shown in the search box if the user didn't enter any query
             */
            placeholder: {
                type: String,
                value: 'Search'
            },

            /**
             * Text shown if no results are found. Use this property to localize the element.
             */
            noResultsText: {
                type: String,
                value: 'No matching results found.'
            },

            /**
             * Text for the more button to load more data. Use this property to localize the element.
             */
            moreButton: {
                type: String,
                value: 'More'
            },

            /**
             * Text for the reset button in the filter dialog. Use this property to localize the element.
             */
            resetButton: String,

            /**
             * Text for the save button in the filter dialog. Use this property to localize the element.
             */
            saveButton: String,

            /**
             * Label shown if no values are selected for a filter. Use this property to localize the element.
             */
            noValuesLabel: String,
            

            _hasItems: {
                type: Boolean,
                computed: '_computeHasItems(items)',
                value: false
            }
        }
    }

    getPaperSearchBarInstance() {
        return this.$.paperSearchBar;
	}

    // Private methods
    _loadMore() {
        this.count += 20;

        this._updateData();
    }

    _computeHasItems() {
        return typeof items !== 'undefined' && items.length > 0;
    }

    _showNoResults(_hasItems, loading) {
        return !_hasItems && !loading;
    }
    
    _onChangeRequest(newValue, oldValue) {
        // Ignore initial setting of properties (caller is supposed to trigger this call automatically)
        if (typeof oldValue !== 'undefined') {
            // Set back to default to avoid endless listers
            this.count = 20;
            this._updateData();
        }
    }

    _updateData() {
        this.fire('change-request-params');
    }

    _onFilter() {
        this.$.filterDialog.open();
    }

    _onSearch() {
        this.fire('search');
    }

    // Counts the selected filters
    _getNrSelectedFilters(selectedFilters) {
        if (Object.keys(selectedFilters).length <= 0) {
            return 0;
        }

        var nrSelectedFilters = Object.keys(selectedFilters)
            .map(function(key) {
                // Returns number of selected value for a filter
                return selectedFilters[key].length;
            })
            .reduce(function(sum, value) {
                // Sum up the selected values across filters
                return sum + value;
            });

        return nrSelectedFilters;
    }

    _disableFilterButton(filters) {
        return !(filters && filters.length > 0);
    }
}
customElements.define('paper-search-panel', PaperSearchPanel);